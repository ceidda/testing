package uy.edu.cei.dda.testing;

public class ProductController {

	private ProductService productService;

	public ProductController() {
		this.productService = new ProductServiceImpl();
	}
	
	public ProductController(ProductService productService) {
		this.productService = productService;
	}

	public void something() {
		System.out.println(this.productService.getProductById(1));
	}
}
