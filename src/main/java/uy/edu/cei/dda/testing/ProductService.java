package uy.edu.cei.dda.testing;

public interface ProductService {

	public String getProductById(Integer id);
	
}
