package uy.edu.cei.dda.testing;

import org.junit.Test;
import org.mockito.Mockito;

public class ProductControllerTest {

	@Test
	public void test() {
		ProductService ps = Mockito.mock(ProductService.class);
		Mockito.when(ps.getProductById(1)).thenReturn("mars");
		ProductController pc = new ProductController(ps);
		pc.something();
		Mockito.verify(ps, Mockito.times(1)).getProductById(1);
	}

}
